class AddFeaturedToCauses < ActiveRecord::Migration[6.1]
  def change
    #adding the column featured to causes as a filter to show in index
    add_column :causes, :featured, :boolean
  end
end
