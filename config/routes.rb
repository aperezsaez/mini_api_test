Rails.application.routes.draw do
    # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
    root to: 'application#hello'
    #statistics show the total amount of causes
    #causes which has more donations than expected amount
    #and the total sum of donations
    get 'statistics', to: 'causes#statistics'
    resources :causes, only:[:index, :show, :update]
    resources :donations, only:[:create, :index, :destroy]
end
