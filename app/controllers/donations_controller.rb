class DonationsController < ApplicationController

    #index receives a query string with the id of the cause we want to get so i list the donations of the especific cause
    def index
        ## TODO: Return a list of donations filtering by cause
        @cause = Cause.find(params[:cause])
        render json: @cause.donations, status: 200
    end

    #create a donation with it's params, is expected to get the cause_id in the params as the test says so i just added cause_id to the permited params 
    def create
        # TODO: Create a donation
        @donation = Donation.new(donation_params)
        @donation.save
        render json: @donation.to_json, status: 201
    end

    #destroy receives an id identifying a specific donations via params, so i use the id to identify the donation to destroy and destroy it 
    def destroy
        # TODO: Remove a donation from database
        @donation = Donation.find(params[:id])
        @donation.destroy
        render json: @donation.to_json, status: 201
    end

    private
    # Only allow a list of trusted parameters through.
    # i changed the previews name of cause_params to donation_params
    def donation_params
        params.fetch(:donation, {}).permit(:media, :quantity, :cause_id)
    end
end
