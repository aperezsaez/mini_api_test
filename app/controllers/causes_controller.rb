class CausesController < ApplicationController

    before_action :set_cause, only: %i[show update]
    #index gets all causes with it's organization where featured == true as expected in the last point, also it has eager_load to do one request to the data base
    def index
        causes = Cause.eager_load(:organization).where(featured: true)
        render json: causes.to_json(include: [:organization]), status: 200
    end

    #statistics get the total causes in the app, causes with more donations than the expected amoun and the total amoun of donations
    def statistics
        render json: {
            total_causes: Cause.all.count,
            more_donations_than_expected: Cause.eager_load(:donations).where('causes.id = donations.cause_id').select{ |c| c.expected_amount < c.donations.sum(&:quantity) }.count,
            total_donations: Donation.all.sum(&:quantity)
        }
    end
    
    #show gets the selected cause with the organization it belongs, it's reports and donations
    def show
        ## TODO: Return a complete cause including organization, reports and donations
        render json: @cause.to_json(include: [:organization, :reports, :donations]), status: 200
    end

    #update the selected cause
    def update
        ## TODO: Update a cause attributes
        @cause.update(cause_params)
        render json:  @cause.to_json, status: 201
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_cause
        @cause = Cause.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cause_params
        params.fetch(:cause, {}).permit(:title, :description, :expected_amount, :featured )
    end
end
